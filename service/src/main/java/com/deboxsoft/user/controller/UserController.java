/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-service
 *  File       : UserController.java
 *  ClassName  : UserController
 *  Modified   : 150514511
 */

package com.deboxsoft.user.controller;


import com.deboxsoft.user.DeboxSecurityConfig;
import com.deboxsoft.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class UserController {
    private UserManager userManager;


    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/login")
    public void login(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter(DeboxSecurityConfig.USERNAME_PARAMETER);
        String password = request.getParameter(DeboxSecurityConfig.PASSWORD_PARAMETER);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        //try {
        //    final Authentication authentication = authenticationManager.authenticate(authenticationToken);
        //}catch (BadCredentialsException e){
            //return "{}";
        //}

        //return null;
    }

}
