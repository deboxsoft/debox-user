package com.deboxsoft.user;

import com.deboxsoft.user.login.LoginFailureHandler;
import com.deboxsoft.user.login.LoginSuccessHandler;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
@EnableWebSecurity
public class DeboxSecurityConfig extends WebSecurityConfigurerAdapter {

    public void configure(HttpSecurity http) throws Exception {
        // @formatter:off
        http.authorizeRequests()
                .antMatchers("/", "/assets/**").permitAll()
                .antMatchers("/api/login").permitAll()
                .anyRequest().fullyAuthenticated()
            .and().formLogin()
                .loginPage("/login")
                .permitAll()
            .and().logout()
                .logoutUrl("/logout")
                .deleteCookies("remember-me")
                .logoutSuccessUrl("/")
                .permitAll()
            .and().rememberMe();

        // @formatter:on
    }


    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
    }

    public static String USERNAME_PARAMETER = "debox_username";
    public static String PASSWORD_PARAMETER = "debox_password";

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter() {
        UsernamePasswordAuthenticationFilter authenticationFilter = new UsernamePasswordAuthenticationFilter();
        authenticationFilter.setUsernameParameter(USERNAME_PARAMETER);
        authenticationFilter.setPasswordParameter(PASSWORD_PARAMETER);
        authenticationFilter.setAuthenticationSuccessHandler(loginSuccessHandler());
        authenticationFilter.setAuthenticationFailureHandler(loginFailureHandler());
        authenticationFilter.setAuthenticationManager(authenticationManager());
        return authenticationFilter;
    }

    @Bean
    public AuthenticationManager authenticationManager(){
        return new DeboxAuthenticationManager();
    }

    private LoginSuccessHandler loginSuccessHandler(){
        return new LoginSuccessHandler();
    }

    private LoginFailureHandler loginFailureHandler() {
        return new LoginFailureHandler();
    }


}
