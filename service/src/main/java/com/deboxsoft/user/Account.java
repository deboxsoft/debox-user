package com.deboxsoft.user;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;


public class Account implements UserDetail<Group> {
    public interface ProfileView{}

    private Integer id;
    private String username;

    private String password;

    private String email;

    private String name;

    private boolean enabled;



    public Integer getId() {
        return null;
    }

    public String getEmail() {
        return null;
    }

    public String getName() {
        return null;
    }

    public Collection<Group> getGroup() {

        return null;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @JsonView(ProfileView.class)
    public String getPassword() {
        return null;
    }

    @JsonView(ProfileView.class)
    public String getUsername() {
        return null;
    }

    public boolean isAccountNonExpired() {
        return false;
    }

    public boolean isAccountNonLocked() {
        return false;
    }

    public boolean isCredentialsNonExpired() {
        return false;
    }

    public boolean isEnabled() {
        return false;
    }

}
