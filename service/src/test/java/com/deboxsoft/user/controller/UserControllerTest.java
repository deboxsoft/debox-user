package com.deboxsoft.user.controller;

import com.deboxsoft.user.DeboxSecurityConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {DeboxSecurityConfig.class, TestConfigurationHelper.class})
@WebAppConfiguration
public class UserControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AuthenticationManager authenticationManager;

    private MockMvc mvc;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        UserController userController = new UserController();
        mvc = MockMvcBuilders.standaloneSetup(userController)
                .apply(springSecurity()).build();
    }

    @Test
    public void testLogin() throws Exception {
        mvc.perform(formLogin()
                .user(DeboxSecurityConfig.USERNAME_PARAMETER, "user")
                .password(DeboxSecurityConfig.PASSWORD_PARAMETER, "test"));
    }

}