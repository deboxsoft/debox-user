/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-core
 *  File       : User.java
 *  ClassName  : User
 *  Modified   : 15025706
 */

package com.deboxsoft.user;

import java.io.Serializable;
import java.text.Normalizer;
import java.util.Collection;

public interface User<T extends Group> extends Serializable{
    Integer getId();

    String getEmail();

    String getName();

    Collection<T> getGroup();

    public static String slugify(String name) {
        String slug = Normalizer.normalize(name, Normalizer.Form.NFKD).replaceAll("[:/?#@!$&'()*+,;=%\\\\\\[\\]]", "-");
        if (slug.length() > 127) {
            slug = slug.substring(0, 126);
        }
        return slug.toLowerCase();
    }

    // TODO Securetoken generateor atlassian-secure-random
    /*public static String generateSlug(SecureTokenGenerator tokenGenerator){
        String slug = tokenGenerator.generateToken();
        if(slug.length()>127 ){
            slug = slug.substring(0, 126);
        }
        return slug;
    }*/
}
