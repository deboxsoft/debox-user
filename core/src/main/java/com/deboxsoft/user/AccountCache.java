/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-core
 *  File       : AccountCache.java
 *  ClassName  : AccountCache
 *  Modified   : 15036802
 */

package com.deboxsoft.user;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;

public interface AccountCache {

    UserCache getUserCache();

    void setUserCache(UserCache userCache);

    UserDetails getUserFromCache(Integer id);


}
