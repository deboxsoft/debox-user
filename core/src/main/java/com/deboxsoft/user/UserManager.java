/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-core
 *  File       : UserService.java
 *  ClassName  : UserService
 *  Modified   : 15025708
 */

package com.deboxsoft.user;

import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserManager extends UserDetailsService {
    void createUser(String username, String password, String name, String email) throws UserExistException, EmailExistException;

    void updateUser(Integer id, String name, String email);

    void updateUser(User user);

    void removeUser(Integer id);

    void changePassword(Integer id, String oldPassword, String newPassword);

    void changePassword(User user, String oldPassword, String newPassword);

    boolean userExist(String username);

    boolean userMailExist(String email);

    void setUserCache(UserCache userCache);

    UserDetail getUserDetail(String username);
}
