/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : EmailExistException.java
 *  ClassName  : EmailExistException
 *  Modified   : 15036407
 */

package com.deboxsoft.user;

public class EmailExistException extends Exception {

    private static final long serialVersionUID = -8710909732498531043L;

    public EmailExistException(String message) {
        super(message);
    }

    public EmailExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
