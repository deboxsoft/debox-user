/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-core
 *  File       : GroupManager.java
 *  ClassName  : GroupManager
 *  Modified   : 15036301
 */

package com.deboxsoft.user;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface GroupManager {

    List<Group> findAllGroups();

    List<User> findUsersInGroup(Group group);

    void createGroup(String groupName, List<GrantedAuthority> authorities);

    void renameGroup(Integer idGroup, String newName);

    void addMemberGroup(Integer idUser, Integer idGroup);

    void removeMemberGroup(Integer idUser, Integer idGroup);

    void changePermission(Integer idGroup, List<GrantedAuthority> authorities);

}
