package com.deboxsoft.user;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetail<T extends Group> extends User<T>, UserDetails {
}
