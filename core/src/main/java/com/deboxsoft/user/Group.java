/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-core
 *  File       : Group.java
 *  ClassName  : Group
 *  Modified   : 15025917
 */

package com.deboxsoft.user;

import java.io.Serializable;
import java.util.Collection;

public interface Group<T extends User> extends Serializable{
    Integer getId();

    String getName();

    Collection<T> getMember();
}
