/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : UsernameExistException.java
 *  ClassName  : UsernameExistException
 *  Modified   : 15036407
 */

package com.deboxsoft.user;

public class UserExistException extends Exception {
    private static final long serialVersionUID = 3225106287892901924L;

    public UserExistException(String message) {
        super(message);
    }

    public UserExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
