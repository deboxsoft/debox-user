/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : UserManagerImpl.java
 *  ClassName  : UserManagerImpl
 *  Modified   : 15036317
 */

package com.deboxsoft.user;

import com.deboxsoft.app.i18n.DboxI18nService;
import com.deboxsoft.user.domain.GroupEntity;
import com.deboxsoft.user.domain.UserEntity;
import com.deboxsoft.user.repository.GroupRepo;
import com.deboxsoft.user.repository.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.cache.NullUserCache;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("profileManager")
public class ProfileManager implements UserManager, GroupManager {
    private static final Logger logger = LoggerFactory.getLogger(ProfileManager.class);
    @Autowired
    private GroupRepo groupRepo;
    @Autowired
    private UserRepo userRepo;
    private UserCache userCache = new NullUserCache();
    @Autowired
    private DboxI18nService i18nService;

    public ProfileManager() {
    }

    @Autowired
    public ProfileManager(GroupRepo groupRepo, UserRepo userRepo, DboxI18nService i18nService) {
        this.groupRepo = groupRepo;
        this.userRepo = userRepo;
        this.i18nService = i18nService;
    }

    public void createUser(String username, String password, String name, String email)
            throws UserExistException, EmailExistException {
        if (userExist(username)) {
            throw new UserExistException(i18nService.getMessage("security.user.exception.exist", "username sudah terdaftar {0}", username));
        }
        if (userMailExist(email)) throw new EmailExistException("email sudah terdaftar");
        UserEntity deboxUser = UserEntity.builder()
                .username(username)
                .password(password)
                .name(name)
                .email(email).build();
        userRepo.save(deboxUser);
    }

    public void updateUser(Integer id, String name, String email) {
        UserEntity deboxUser = userRepo.findOne(id);
        deboxUser.setName(name);
        deboxUser.setEmail(email);
        userRepo.save(deboxUser);
    }

    public void updateUser(User user) {
        updateUser(user.getId(), user.getName(), user.getEmail());
    }

    public void removeUser(Integer id) {
        userRepo.delete(id);
    }

    public void changePassword(Integer id, String oldPassword, String newPassword) {
        UserEntity deboxUser = userRepo.findOne(id);
        if (deboxUser.getPassword().equals(oldPassword)){
            deboxUser.setPassword(newPassword);
            userRepo.save(deboxUser);
        }else{
            logger.warn("Password user {} {} not match with old password {}", deboxUser.getUsername(),
                    deboxUser.getPassword(), oldPassword);
        }
    }

    public void changePassword(User user, String oldPassword, String newPassword) {
       changePassword(user.getId(), oldPassword, newPassword);
    }

    public boolean userExist(String username) {
        return userRepo.existUsername(username);
    }

    public boolean userMailExist(String email) {
        return userRepo.existEmail(email);
    }

    public void setUserCache(UserCache userCache) {
        Assert.notNull(userCache, "userCache can not be null");
        this.userCache = userCache;
    }

    public UserDetail getUserDetail(String username) throws UsernameNotFoundException{
        Optional<UserEntity> userEntityOptional = Optional.of(userRepo.findByUsername(username));
        if(!userEntityOptional.isPresent()) throw new UsernameNotFoundException("user not found");
        return userEntityOptional.get();
    }

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepo.findByUsername(username);
    }

    public List<Group> findAllGroups() {
        Collection<GroupEntity> deboxGroups = groupRepo.findAll();
        return deboxGroups.stream()
                .map(deboxGroup -> (Group) deboxGroup)
                .collect(Collectors.toList());
    }

    public List<User> findUsersInGroup(Group group) {

        Collection<UserEntity> deboxGroups = groupRepo.findOne(group.getId()).getMember();
        return deboxGroups.stream()
                .map(user -> (User) user)
                .collect(Collectors.toList());
    }

    public void createGroup(String groupName, List<GrantedAuthority> authorities) {
        GroupEntity.builder()
                .name(groupName);


    }

    public void renameGroup(Integer idGroup, String newName) {

    }


    public void addMemberGroup(Integer idUser, Integer idGroup) {

    }

    public void removeMemberGroup(Integer idUser, Integer idGroup) {

    }

    public void changePermission(Integer idGroup, List<GrantedAuthority> authorities) {

    }

}
