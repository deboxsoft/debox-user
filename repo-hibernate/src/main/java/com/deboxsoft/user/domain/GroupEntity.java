/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : GroupEntity.java
 *  ClassName  : GroupEntity
 *  Modified   : 15036007
 */

package com.deboxsoft.user.domain;

import com.deboxsoft.user.Group;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Collection;

@Entity(name = "Group")
@Data
@NoArgsConstructor
public class GroupEntity implements Group<UserEntity> {
    private static final long serialVersionUID = 7711485049945342597L;

    @Id
    private Integer id;

    @Column(unique = true)
    private String name;

    @ManyToMany(mappedBy = "group")
    private Collection<UserEntity> member;

    @Builder
    public GroupEntity(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
