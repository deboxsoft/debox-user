/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : User.java
 *  ClassName  : User
 *  Modified   : 15025614
 */

package com.deboxsoft.user.domain;

import com.deboxsoft.user.UserDetail;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Collection;

@Entity(name = "User")
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"enabled", "password", "authorities", "deboxGroups"})
public class UserEntity implements UserDetail<GroupEntity> {
    private static final long serialVersionUID = 518876805869553510L;

    @Id
    private Integer id;

    private String username;

    private String password;

    private String email;

    private String name;

    private boolean enabled;

    @Singular
    @ManyToMany
    @JoinTable(
            name = "dbx_member",
            joinColumns = {@JoinColumn( name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id")}
    )
    private Collection<GroupEntity> deboxGroups;

    @Transient
    private Collection<? extends GrantedAuthority> authorities;

    @Builder
    public UserEntity(Integer id, String username, String password, String email, String name, boolean enabled) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.enabled = enabled;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public Collection<GroupEntity> getGroup() {
        return deboxGroups;
    }
}
