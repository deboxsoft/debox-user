/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : DboxAuthority.java
 *  ClassName  : DboxAuthority
 *  Modified   : 15036802
 */

package com.deboxsoft.user.domain;

import org.springframework.security.core.GrantedAuthority;


public class DeboxAuthority implements GrantedAuthority {
    private static final long serialVersionUID = -178332365080263463L;

    public String getAuthority() {
        return null;
    }
}
