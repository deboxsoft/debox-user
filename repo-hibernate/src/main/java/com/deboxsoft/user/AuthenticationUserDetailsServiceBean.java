/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : AuthenticationUserDetailsServiceBean.java
 *  ClassName  : AuthenticationUserDetailsServiceBean
 *  Modified   : 15036400
 */

package com.deboxsoft.user;

import org.springframework.security.core.*;

import java.util.Collection;

public class AuthenticationUserDetailsServiceBean implements Authentication {
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public Object getCredentials() {
        return null;
    }

    public Object getDetails() {
        return null;
    }

    public Object getPrincipal() {
        return null;
    }

    public boolean isAuthenticated() {
        return false;
    }

    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    public String getName() {
        return null;
    }
}
