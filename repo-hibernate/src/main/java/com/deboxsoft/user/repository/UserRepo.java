/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxuser
 *  Module     : dboxuser-repo-hibernate
 *  File       : UserRepo.java
 *  ClassName  : UserRepo
 *  Modified   : 15025921
 */

package com.deboxsoft.user.repository;

import com.deboxsoft.user.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends JpaRepository<UserEntity, Integer> {

    UserEntity findByUsername(String username);

    @Query("SELECT 1 FROM User u WHERE u.username = ?1")
    boolean existUsername(String username);

    @Query("SELECT 1 FROM User u WHERE u.email = ?1")
    boolean existEmail(String email);
}
