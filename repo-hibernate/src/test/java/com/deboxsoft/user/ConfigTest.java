package com.deboxsoft.user;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.deboxsoft")
public class ConfigTest {
}
