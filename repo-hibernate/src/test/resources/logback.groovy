import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-db
 *  Module     : dboxdb-hibernate
 *  File       : logback.groovy
 *  ClassName  : logback.groovy
 *  Modified   : 15036006
 */

appender ("console", ConsoleAppender) {
    encoder(PatternLayoutEncoder){
        pattern = "[%-5level|%d{dd/MM HH:mm:ss:SSS}|%thread|%logger{36}:%line] - %msg%n"
    }
}

root (INFO, ["console"])
