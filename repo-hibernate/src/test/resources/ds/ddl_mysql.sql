
    alter table DBX_MEMBER 
        drop 
        foreign key FK_fc1se53bhmd8ls0ra3se526it;

    alter table DBX_MEMBER 
        drop 
        foreign key FK_8y7jrsvmhl4ijpguc0fbo63v7;

    drop table if exists DBX_GROUP;

    drop table if exists DBX_MEMBER;

    drop table if exists DBX_USER;

    create table DBX_GROUP (
        ID integer not null,
        NAME varchar(255),
        primary key (ID)
    ) ENGINE=InnoDB;

    create table DBX_MEMBER (
        USER_ID integer not null,
        GROUP_ID integer not null
    ) ENGINE=InnoDB;

    create table DBX_USER (
        ID integer not null,
        EMAIL varchar(255),
        ENABLED bit not null,
        NAME varchar(255),
        PASSWORD varchar(255),
        USERNAME varchar(255),
        primary key (ID)
    ) ENGINE=InnoDB;

    alter table DBX_GROUP 
        add constraint UK_osgyodu2tixk4c1j7mrwy0jyy  unique (NAME);

    alter table DBX_MEMBER 
        add constraint FK_fc1se53bhmd8ls0ra3se526it 
        foreign key (GROUP_ID) 
        references DBX_GROUP (ID);

    alter table DBX_MEMBER 
        add constraint FK_8y7jrsvmhl4ijpguc0fbo63v7 
        foreign key (USER_ID) 
        references DBX_USER (ID);
