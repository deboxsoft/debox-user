
    alter table DBX_MEMBER 
        drop constraint FK_fc1se53bhmd8ls0ra3se526it;

    alter table DBX_MEMBER 
        drop constraint FK_8y7jrsvmhl4ijpguc0fbo63v7;

    drop table DBX_GROUP if exists;

    drop table DBX_MEMBER if exists;

    drop table DBX_USER if exists;

    create table DBX_GROUP (
        ID integer not null,
        NAME varchar(255),
        primary key (ID)
    );

    create table DBX_MEMBER (
        USER_ID integer not null,
        GROUP_ID integer not null
    );

    create table DBX_USER (
        ID integer not null,
        EMAIL varchar(255),
        ENABLED boolean not null,
        NAME varchar(255),
        PASSWORD varchar(255),
        USERNAME varchar(255),
        primary key (ID)
    );

    alter table DBX_GROUP 
        add constraint UK_osgyodu2tixk4c1j7mrwy0jyy  unique (NAME);

    alter table DBX_MEMBER 
        add constraint FK_fc1se53bhmd8ls0ra3se526it 
        foreign key (GROUP_ID) 
        references DBX_GROUP;

    alter table DBX_MEMBER 
        add constraint FK_8y7jrsvmhl4ijpguc0fbo63v7 
        foreign key (USER_ID) 
        references DBX_USER;
